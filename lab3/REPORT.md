# Лабораторная работа №3. Выделение контуров на изображении

**Выполнил:**

_Максимов Денис Б19-514_

**Задание:**

_Оператор Собеля._

### Изображение 1

#### Исходное

![](./images/face.jpeg)

#### Полутоновое

![](./output_images/s_face.png)

#### Выделение по XY

![](./output_images/s_face_xy.png)

![](./output_images/face_xy.png)

#### Выделение по X

![](./output_images/face_x.png)

#### Выделение по Y

![](./output_images/face_y.png)

### Изображение 2

#### Исходное

![](./images/letter2.bmp)

#### Полутоновое

![](./output_images/s_letter2.png)

#### Выделение по XY

![](./output_images/s_letter2_xy.png)

![](./output_images/letter2_xy.png)

#### Выделение по X

![](./output_images/letter2_x.png)

#### Выделение по Y

![](./output_images/letter2_y.png)

### Изображение 2

#### Исходное

![](./images/pikachu.bmp)

#### Полутоновое

![](./output_images/s_pikachu.png)

#### Выделение по XY

![](./output_images/s_pikachu_xy.png)

![](./output_images/pikachu_xy.png)

#### Выделение по X

![](./output_images/pikachu_x.png)

#### Выделение по Y

![](./output_images/pikachu_y.png)