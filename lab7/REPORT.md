# Лабораторная работа №7. Текстурный анализ и контратирование

**Выполнил:** 

_Максимов Денис Б19-514_

**Задание:**

_Матрица Харалика с d = 3, phi={45, 135, 225, 315}. Расчёт СКО и мат. ожидания. Для контратирования применялся метод степенного преобразования передаточной функции._ 

### Изображение cloth

![](./resources/cloth.jpeg)

![](./results/s_cloth.jpeg)


#### Признаки

+ Мат. ожиадние серого по i = 1123691478.0

+ Мат. ожиадние серого по j = 1123718572.0

+ СКО серого по i = 1.2833693327827372e+25

+ СКО серого по j = 1.2833074468315366e+25


#### Матрица Харалика

![](./results/haralik_cloth.jpeg)


#### Гистограмма

![](./hists/cloth.png)


#### Контрастированные изображения с матрицами Харалика при разных параметрах

##### Константы c = 1, f_0 = 0, γ = 0.1

+ ![](./results/cloth_1.0_0.0_0.1.png)

+ ![](./results/cloth_1.0_0.0_0.1_rgb.png)

+ ![](./results/haralik_cloth_1_0_0.1.png)

+ ![](./hists/cloth_1_0_0.1.png)

##### Константы c = 1, f_0 = 0, γ = 0.5

+ ![](./results/cloth_1.0_0.0_0.5.png)

+ ![](./results/cloth_1.0_0.0_0.5_rgb.png)

+ ![](./results/haralik_cloth_1_0_0.5.png)

+ ![](./hists/cloth_1_0_0.5.png)

##### Константы c = 1, f_0 = 0, γ = 1.5

+ ![](./results/cloth_1.0_0.0_1.5.png)

+ ![](./results/cloth_1.0_0.0_1.5_rgb.png)

+ ![](./results/haralik_cloth_1_0_1.5.png)

+ ![](./hists/cloth_1_0_1.5.png)

##### Константы c = 1, f_0 = 0, γ = 2

+ ![](./results/cloth_1.0_0.0_2.0.png)

+ ![](./results/cloth_1.0_0.0_2.0_rgb.png)

+ ![](./results/haralik_cloth_1_0_2.png)

+ ![](./hists/cloth_1_0_2.png)

##### Константы c = 1, f_0 = 0, γ = 2.5

+ ![](./results/cloth_1.0_0.0_2.5.png)

+ ![](./results/cloth_1.0_0.0_2.5_rgb.png)

+ ![](./results/haralik_cloth_1_0_2.5.png)

+ ![](./hists/cloth_1_0_2.5.png)

##### Константы c = 0.1, f_0 = 0, γ = 1

+ ![](./results/cloth_0.1_0.0_1.0.png)

+ ![](./results/cloth_0.1_0.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_0.1_0_1.png)

+ ![](./hists/cloth_0.1_0_1.png)

##### Константы c = 0.8, f_0 = 0, γ = 1

+ ![](./results/cloth_0.8_0.0_1.0.png)

+ ![](./results/cloth_0.8_0.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_0.8_0_1.png)

+ ![](./hists/cloth_0.8_0_1.png)

##### Константы c = 2, f_0 = 0, γ = 1

+ ![](./results/cloth_2.0_0.0_1.0.png)

+ ![](./results/cloth_2.0_0.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_2_0_1.png)

+ ![](./hists/cloth_2_0_1.png)

##### Константы c = 0.5, f_0 = 0, γ = 1

+ ![](./results/cloth_0.5_0.0_1.0.png)

+ ![](./results/cloth_0.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_0.5_0_1.png)

+ ![](./hists/cloth_0.5_0_1.png)

##### Константы c = 1.5, f_0 = 0, γ = 1

+ ![](./results/cloth_1.5_0.0_1.0.png)

+ ![](./results/cloth_1.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_1.5_0_1.png)

+ ![](./hists/cloth_1.5_0_1.png)

##### Константы c = 1, f_0 = -10, γ = 1

+ ![](./results/cloth_1.0_-10.0_1.0.png)

+ ![](./results/cloth_1.0_-10.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_1_-10_1.png)

+ ![](./hists/cloth_1_-10_1.png)

##### Константы c = 1, f_0 = 10, γ = 1

+ ![](./results/cloth_1.0_10.0_1.0.png)

+ ![](./results/cloth_1.0_10.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_1_10_1.png)

+ ![](./hists/cloth_1_10_1.png)

##### Константы c = 1, f_0 = 100, γ = 1

+ ![](./results/cloth_1.0_100.0_1.0.png)

+ ![](./results/cloth_1.0_100.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_1_100_1.png)

+ ![](./hists/cloth_1_100_1.png)

##### Константы c = 1, f_0 = 50, γ = 1

+ ![](./results/cloth_1.0_50.0_1.0.png)

+ ![](./results/cloth_1.0_50.0_1.0_rgb.png)

+ ![](./results/haralik_cloth_1_50_1.png)

+ ![](./hists/cloth_1_50_1.png)

### Изображение karamel

![](./resources/karamel.jpeg)

![](./results/s_karamel.jpeg)


#### Признаки

+ Мат. ожиадние серого по i = 175966847.0

+ Мат. ожиадние серого по j = 175969556.0

+ СКО серого по i = 3.1562233397089262e+22

+ СКО серого по j = 3.156126161990415e+22


#### Матрица Харалика

![](./results/haralik_karamel.jpeg)


#### Гистограмма

![](./hists/karamel.png)


#### Контрастированные изображения с матрицами Харалика при разных параметрах

##### Константы c = 1, f_0 = 0, γ = 0.1

+ ![](./results/karamel_1.0_0.0_0.1.png)

+ ![](./results/karamel_1.0_0.0_0.1_rgb.png)

+ ![](./results/haralik_karamel_1_0_0.1.png)

+ ![](./hists/karamel_1_0_0.1.png)

##### Константы c = 1, f_0 = 0, γ = 0.5

+ ![](./results/karamel_1.0_0.0_0.5.png)

+ ![](./results/karamel_1.0_0.0_0.5_rgb.png)

+ ![](./results/haralik_karamel_1_0_0.5.png)

+ ![](./hists/karamel_1_0_0.5.png)

##### Константы c = 1, f_0 = 0, γ = 1.5

+ ![](./results/karamel_1.0_0.0_1.5.png)

+ ![](./results/karamel_1.0_0.0_1.5_rgb.png)

+ ![](./results/haralik_karamel_1_0_1.5.png)

+ ![](./hists/karamel_1_0_1.5.png)

##### Константы c = 1, f_0 = 0, γ = 2

+ ![](./results/karamel_1.0_0.0_2.0.png)

+ ![](./results/karamel_1.0_0.0_2.0_rgb.png)

+ ![](./results/haralik_karamel_1_0_2.png)

+ ![](./hists/karamel_1_0_2.png)

##### Константы c = 1, f_0 = 0, γ = 2.5

+ ![](./results/karamel_1.0_0.0_2.5.png)

+ ![](./results/karamel_1.0_0.0_2.5_rgb.png)

+ ![](./results/haralik_karamel_1_0_2.5.png)

+ ![](./hists/karamel_1_0_2.5.png)

##### Константы c = 0.1, f_0 = 0, γ = 1

+ ![](./results/karamel_0.1_0.0_1.0.png)

+ ![](./results/karamel_0.1_0.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_0.1_0_1.png)

+ ![](./hists/karamel_0.1_0_1.png)

##### Константы c = 0.8, f_0 = 0, γ = 1

+ ![](./results/karamel_0.8_0.0_1.0.png)

+ ![](./results/karamel_0.8_0.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_0.8_0_1.png)

+ ![](./hists/karamel_0.8_0_1.png)

##### Константы c = 2, f_0 = 0, γ = 1

+ ![](./results/karamel_2.0_0.0_1.0.png)

+ ![](./results/karamel_2.0_0.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_2_0_1.png)

+ ![](./hists/karamel_2_0_1.png)

##### Константы c = 0.5, f_0 = 0, γ = 1

+ ![](./results/karamel_0.5_0.0_1.0.png)

+ ![](./results/karamel_0.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_0.5_0_1.png)

+ ![](./hists/karamel_0.5_0_1.png)

##### Константы c = 1.5, f_0 = 0, γ = 1

+ ![](./results/karamel_1.5_0.0_1.0.png)

+ ![](./results/karamel_1.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_1.5_0_1.png)

+ ![](./hists/karamel_1.5_0_1.png)

##### Константы c = 1, f_0 = -10, γ = 1

+ ![](./results/karamel_1.0_-10.0_1.0.png)

+ ![](./results/karamel_1.0_-10.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_1_-10_1.png)

+ ![](./hists/karamel_1_-10_1.png)

##### Константы c = 1, f_0 = 10, γ = 1

+ ![](./results/karamel_1.0_10.0_1.0.png)

+ ![](./results/karamel_1.0_10.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_1_10_1.png)

+ ![](./hists/karamel_1_10_1.png)

##### Константы c = 1, f_0 = 100, γ = 1

+ ![](./results/karamel_1.0_100.0_1.0.png)

+ ![](./results/karamel_1.0_100.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_1_100_1.png)

+ ![](./hists/karamel_1_100_1.png)

##### Константы c = 1, f_0 = 50, γ = 1

+ ![](./results/karamel_1.0_50.0_1.0.png)

+ ![](./results/karamel_1.0_50.0_1.0_rgb.png)

+ ![](./results/haralik_karamel_1_50_1.png)

+ ![](./hists/karamel_1_50_1.png)

### Изображение rock

![](./resources/rock.jpeg)

![](./results/s_rock.jpeg)


#### Признаки

+ Мат. ожиадние серого по i = 110045080.0

+ Мат. ожиадние серого по j = 110060568.0

+ СКО серого по i = 9.224711607188981e+21

+ СКО серого по j = 9.222115533800768e+21


#### Матрица Харалика

![](./results/haralik_rock.jpeg)


#### Гистограмма

![](./hists/rock.png)


#### Контрастированные изображения с матрицами Харалика при разных параметрах

##### Константы c = 1, f_0 = 0, γ = 0.1

+ ![](./results/rock_1.0_0.0_0.1.png)

+ ![](./results/rock_1.0_0.0_0.1_rgb.png)

+ ![](./results/haralik_rock_1_0_0.1.png)

+ ![](./hists/rock_1_0_0.1.png)

##### Константы c = 1, f_0 = 0, γ = 0.5

+ ![](./results/rock_1.0_0.0_0.5.png)

+ ![](./results/rock_1.0_0.0_0.5_rgb.png)

+ ![](./results/haralik_rock_1_0_0.5.png)

+ ![](./hists/rock_1_0_0.5.png)

##### Константы c = 1, f_0 = 0, γ = 1.5

+ ![](./results/rock_1.0_0.0_1.5.png)

+ ![](./results/rock_1.0_0.0_1.5_rgb.png)

+ ![](./results/haralik_rock_1_0_1.5.png)

+ ![](./hists/rock_1_0_1.5.png)

##### Константы c = 1, f_0 = 0, γ = 2

+ ![](./results/rock_1.0_0.0_2.0.png)

+ ![](./results/rock_1.0_0.0_2.0_rgb.png)

+ ![](./results/haralik_rock_1_0_2.png)

+ ![](./hists/rock_1_0_2.png)

##### Константы c = 1, f_0 = 0, γ = 2.5

+ ![](./results/rock_1.0_0.0_2.5.png)

+ ![](./results/rock_1.0_0.0_2.5_rgb.png)

+ ![](./results/haralik_rock_1_0_2.5.png)

+ ![](./hists/rock_1_0_2.5.png)

##### Константы c = 0.1, f_0 = 0, γ = 1

+ ![](./results/rock_0.1_0.0_1.0.png)

+ ![](./results/rock_0.1_0.0_1.0_rgb.png)

+ ![](./results/haralik_rock_0.1_0_1.png)

+ ![](./hists/rock_0.1_0_1.png)

##### Константы c = 0.8, f_0 = 0, γ = 1

+ ![](./results/rock_0.8_0.0_1.0.png)

+ ![](./results/rock_0.8_0.0_1.0_rgb.png)

+ ![](./results/haralik_rock_0.8_0_1.png)

+ ![](./hists/rock_0.8_0_1.png)

##### Константы c = 2, f_0 = 0, γ = 1

+ ![](./results/rock_2.0_0.0_1.0.png)

+ ![](./results/rock_2.0_0.0_1.0_rgb.png)

+ ![](./results/haralik_rock_2_0_1.png)

+ ![](./hists/rock_2_0_1.png)

##### Константы c = 0.5, f_0 = 0, γ = 1

+ ![](./results/rock_0.5_0.0_1.0.png)

+ ![](./results/rock_0.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_rock_0.5_0_1.png)

+ ![](./hists/rock_0.5_0_1.png)

##### Константы c = 1.5, f_0 = 0, γ = 1

+ ![](./results/rock_1.5_0.0_1.0.png)

+ ![](./results/rock_1.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_rock_1.5_0_1.png)

+ ![](./hists/rock_1.5_0_1.png)

##### Константы c = 1, f_0 = -10, γ = 1

+ ![](./results/rock_1.0_-10.0_1.0.png)

+ ![](./results/rock_1.0_-10.0_1.0_rgb.png)

+ ![](./results/haralik_rock_1_-10_1.png)

+ ![](./hists/rock_1_-10_1.png)

##### Константы c = 1, f_0 = 10, γ = 1

+ ![](./results/rock_1.0_10.0_1.0.png)

+ ![](./results/rock_1.0_10.0_1.0_rgb.png)

+ ![](./results/haralik_rock_1_10_1.png)

+ ![](./hists/rock_1_10_1.png)

##### Константы c = 1, f_0 = 100, γ = 1

+ ![](./results/rock_1.0_100.0_1.0.png)

+ ![](./results/rock_1.0_100.0_1.0_rgb.png)

+ ![](./results/haralik_rock_1_100_1.png)

+ ![](./hists/rock_1_100_1.png)

##### Константы c = 1, f_0 = 50, γ = 1

+ ![](./results/rock_1.0_50.0_1.0.png)

+ ![](./results/rock_1.0_50.0_1.0_rgb.png)

+ ![](./results/haralik_rock_1_50_1.png)

+ ![](./hists/rock_1_50_1.png)

### Изображение tiger

![](./resources/tiger.jpeg)

![](./results/s_tiger.jpeg)


#### Признаки

+ Мат. ожиадние серого по i = 82020178.0

+ Мат. ожиадние серого по j = 81985176.0

+ СКО серого по i = 6.05337789540588e+21

+ СКО серого по j = 6.058547757651631e+21


#### Матрица Харалика

![](./results/haralik_tiger.jpeg)


#### Гистограмма

![](./hists/tiger.png)


#### Контрастированные изображения с матрицами Харалика при разных параметрах

##### Константы c = 1, f_0 = 0, γ = 0.1

+ ![](./results/tiger_1.0_0.0_0.1.png)

+ ![](./results/tiger_1.0_0.0_0.1_rgb.png)

+ ![](./results/haralik_tiger_1_0_0.1.png)

+ ![](./hists/tiger_1_0_0.1.png)

##### Константы c = 1, f_0 = 0, γ = 0.5

+ ![](./results/tiger_1.0_0.0_0.5.png)

+ ![](./results/tiger_1.0_0.0_0.5_rgb.png)

+ ![](./results/haralik_tiger_1_0_0.5.png)

+ ![](./hists/tiger_1_0_0.5.png)

##### Константы c = 1, f_0 = 0, γ = 1.5

+ ![](./results/tiger_1.0_0.0_1.5.png)

+ ![](./results/tiger_1.0_0.0_1.5_rgb.png)

+ ![](./results/haralik_tiger_1_0_1.5.png)

+ ![](./hists/tiger_1_0_1.5.png)

##### Константы c = 1, f_0 = 0, γ = 2

+ ![](./results/tiger_1.0_0.0_2.0.png)

+ ![](./results/tiger_1.0_0.0_2.0_rgb.png)

+ ![](./results/haralik_tiger_1_0_2.png)

+ ![](./hists/tiger_1_0_2.png)

##### Константы c = 1, f_0 = 0, γ = 2.5

+ ![](./results/tiger_1.0_0.0_2.5.png)

+ ![](./results/tiger_1.0_0.0_2.5_rgb.png)

+ ![](./results/haralik_tiger_1_0_2.5.png)

+ ![](./hists/tiger_1_0_2.5.png)

##### Константы c = 0.1, f_0 = 0, γ = 1

+ ![](./results/tiger_0.1_0.0_1.0.png)

+ ![](./results/tiger_0.1_0.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_0.1_0_1.png)

+ ![](./hists/tiger_0.1_0_1.png)

##### Константы c = 0.8, f_0 = 0, γ = 1

+ ![](./results/tiger_0.8_0.0_1.0.png)

+ ![](./results/tiger_0.8_0.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_0.8_0_1.png)

+ ![](./hists/tiger_0.8_0_1.png)

##### Константы c = 2, f_0 = 0, γ = 1

+ ![](./results/tiger_2.0_0.0_1.0.png)

+ ![](./results/tiger_2.0_0.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_2_0_1.png)

+ ![](./hists/tiger_2_0_1.png)

##### Константы c = 0.5, f_0 = 0, γ = 1

+ ![](./results/tiger_0.5_0.0_1.0.png)

+ ![](./results/tiger_0.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_0.5_0_1.png)

+ ![](./hists/tiger_0.5_0_1.png)

##### Константы c = 1.5, f_0 = 0, γ = 1

+ ![](./results/tiger_1.5_0.0_1.0.png)

+ ![](./results/tiger_1.5_0.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_1.5_0_1.png)

+ ![](./hists/tiger_1.5_0_1.png)

##### Константы c = 1, f_0 = -10, γ = 1

+ ![](./results/tiger_1.0_-10.0_1.0.png)

+ ![](./results/tiger_1.0_-10.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_1_-10_1.png)

+ ![](./hists/tiger_1_-10_1.png)

##### Константы c = 1, f_0 = 10, γ = 1

+ ![](./results/tiger_1.0_10.0_1.0.png)

+ ![](./results/tiger_1.0_10.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_1_10_1.png)

+ ![](./hists/tiger_1_10_1.png)

##### Константы c = 1, f_0 = 100, γ = 1

+ ![](./results/tiger_1.0_100.0_1.0.png)

+ ![](./results/tiger_1.0_100.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_1_100_1.png)

+ ![](./hists/tiger_1_100_1.png)

##### Константы c = 1, f_0 = 50, γ = 1

+ ![](./results/tiger_1.0_50.0_1.0.png)

+ ![](./results/tiger_1.0_50.0_1.0_rgb.png)

+ ![](./results/haralik_tiger_1_50_1.png)

+ ![](./hists/tiger_1_50_1.png)

