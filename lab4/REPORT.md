# Лабораторная работа №4. Выделение признаков символов

**Выполнил:**

_Максимов Денис Б19-514_

**Задание:**

_Греческий алфавит строчных букв._ 

1. 

 Буква α 

#### Профиль для шрифта ChisatoRegular

![](./hists/α.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/α.png)

![](./output_images/letters/α_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/α.png)

![](./output_images/letters_calibri/α_inverse.png)

2. 

 Буква β 

#### Профиль для шрифта ChisatoRegular

![](./hists/β.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/β.png)

![](./output_images/letters/β_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/β.png)

![](./output_images/letters_calibri/β_inverse.png)

3. 

 Буква γ 

#### Профиль для шрифта ChisatoRegular

![](./hists/γ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/γ.png)

![](./output_images/letters/γ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/γ.png)

![](./output_images/letters_calibri/γ_inverse.png)

4. 

 Буква δ 

#### Профиль для шрифта ChisatoRegular

![](./hists/δ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/δ.png)

![](./output_images/letters/δ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/δ.png)

![](./output_images/letters_calibri/δ_inverse.png)

5. 

 Буква ε 

#### Профиль для шрифта ChisatoRegular

![](./hists/ε.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ε.png)

![](./output_images/letters/ε_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ε.png)

![](./output_images/letters_calibri/ε_inverse.png)

6. 

 Буква ζ 

#### Профиль для шрифта ChisatoRegular

![](./hists/ζ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ζ.png)

![](./output_images/letters/ζ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ζ.png)

![](./output_images/letters_calibri/ζ_inverse.png)

7. 

 Буква η 

#### Профиль для шрифта ChisatoRegular

![](./hists/η.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/η.png)

![](./output_images/letters/η_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/η.png)

![](./output_images/letters_calibri/η_inverse.png)

8. 

 Буква θ 

#### Профиль для шрифта ChisatoRegular

![](./hists/θ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/θ.png)

![](./output_images/letters/θ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/θ.png)

![](./output_images/letters_calibri/θ_inverse.png)

9. 

 Буква ι 

#### Профиль для шрифта ChisatoRegular

![](./hists/ι.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ι.png)

![](./output_images/letters/ι_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ι.png)

![](./output_images/letters_calibri/ι_inverse.png)

10. 

 Буква κ 

#### Профиль для шрифта ChisatoRegular

![](./hists/κ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/κ.png)

![](./output_images/letters/κ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/κ.png)

![](./output_images/letters_calibri/κ_inverse.png)

11. 

 Буква λ 

#### Профиль для шрифта ChisatoRegular

![](./hists/λ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/λ.png)

![](./output_images/letters/λ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/λ.png)

![](./output_images/letters_calibri/λ_inverse.png)

12. 

 Буква μ 

#### Профиль для шрифта ChisatoRegular

![](./hists/μ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/μ.png)

![](./output_images/letters/μ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/μ.png)

![](./output_images/letters_calibri/μ_inverse.png)

13. 

 Буква ν 

#### Профиль для шрифта ChisatoRegular

![](./hists/ν.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ν.png)

![](./output_images/letters/ν_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ν.png)

![](./output_images/letters_calibri/ν_inverse.png)

14. 

 Буква ξ 

#### Профиль для шрифта ChisatoRegular

![](./hists/ξ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ξ.png)

![](./output_images/letters/ξ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ξ.png)

![](./output_images/letters_calibri/ξ_inverse.png)

15. 

 Буква ο 

#### Профиль для шрифта ChisatoRegular

![](./hists/ο.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ο.png)

![](./output_images/letters/ο_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ο.png)

![](./output_images/letters_calibri/ο_inverse.png)

16. 

 Буква π 

#### Профиль для шрифта ChisatoRegular

![](./hists/π.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/π.png)

![](./output_images/letters/π_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/π.png)

![](./output_images/letters_calibri/π_inverse.png)

17. 

 Буква ρ 

#### Профиль для шрифта ChisatoRegular

![](./hists/ρ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ρ.png)

![](./output_images/letters/ρ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ρ.png)

![](./output_images/letters_calibri/ρ_inverse.png)

18. 

 Буква σ 

#### Профиль для шрифта ChisatoRegular

![](./hists/σ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/σ.png)

![](./output_images/letters/σ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/σ.png)

![](./output_images/letters_calibri/σ_inverse.png)

19. 

 Буква τ 

#### Профиль для шрифта ChisatoRegular

![](./hists/τ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/τ.png)

![](./output_images/letters/τ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/τ.png)

![](./output_images/letters_calibri/τ_inverse.png)

20. 

 Буква υ 

#### Профиль для шрифта ChisatoRegular

![](./hists/υ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/υ.png)

![](./output_images/letters/υ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/υ.png)

![](./output_images/letters_calibri/υ_inverse.png)

21. 

 Буква φ 

#### Профиль для шрифта ChisatoRegular

![](./hists/φ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/φ.png)

![](./output_images/letters/φ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/φ.png)

![](./output_images/letters_calibri/φ_inverse.png)

22. 

 Буква χ 

#### Профиль для шрифта ChisatoRegular

![](./hists/χ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/χ.png)

![](./output_images/letters/χ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/χ.png)

![](./output_images/letters_calibri/χ_inverse.png)

23. 

 Буква ψ 

#### Профиль для шрифта ChisatoRegular

![](./hists/ψ.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ψ.png)

![](./output_images/letters/ψ_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ψ.png)

![](./output_images/letters_calibri/ψ_inverse.png)

24. 

 Буква ω 

#### Профиль для шрифта ChisatoRegular

![](./hists/ω.png)

#### Буква в шрифте ChisatoRegular

![](./output_images/letters/ω.png)

![](./output_images/letters/ω_inverse.png)

#### Буква в шрифте Calibri

![](./output_images/letters_calibri/ω.png)

![](./output_images/letters_calibri/ω_inverse.png)


#### Признаки

Полный файл report.csv

| Буква | Вес (масса черного) | "Удельный вес (вес, нормированный к площади)" | Координаты центра тяжести                  | Нормированные координаты центра тяжести      | Осевые моменты инерции по горизонтали и вертикали | Нормированные осевые моменты инерции          |
|-------|---------------------|-----------------------------------------------|--------------------------------------------|----------------------------------------------|---------------------------------------------------|-----------------------------------------------|
| α     | 1376                | 0.6142857142857143                            | "(15.430232558139535, 34.5)"               | "(0.4654913728432108, 0.4855072463768116)"   | "(1190.25, 12.25)"                                | "(0.20091998649561107, 0.4855072463768116)"   |
| β     | 988                 | 0.6753246753246753                            | "(8.492914979757085, 36.233805668016196)"  | "(0.4162730544309492, 0.46360270615810784)"  | "(827.4939363454572, 332.47166913897956)"         | "(0.13155706460182148, 0.46360270615810784)"  |
| γ     | 891                 | 0.6364285714285715                            | "(9.484848484848484, 36.166105499438835)"  | "(0.44657097288676234, 0.5096537028904179)"  | "(831.3934720694914, 294.6751780178642)"          | "(0.15686669284330026, 0.5096537028904179)"   |
| δ     | 894                 | 0.6721804511278195                            | "(9.12751677852349, 35.17785234899329)"    | "(0.4515287099179716, 0.4953311934636709)"   | "(889.360490518445, 295.0786113238143)"           | "(0.16904780279765158, 0.4953311934636709)"   |
| ε     | 643                 | 0.48345864661654137                           | "(6.0279937791601865, 35.2706065318818)"   | "(0.2793329877311215, 0.49667545698379423)"  | "(1137.6719837271344, 298.2738499790784)"         | "(0.21624633790669728, 0.49667545698379423)"  |
| ζ     | 662                 | 0.3782857142857143                            | "(12.531722054380664, 33.81570996978852)"  | "(0.48048841893252764, 0.4755899995621525)"  | "(972.459944688347, 96.34816221100577)"           | "(0.17601084971734787, 0.4755899995621525)"   |
| η     | 955                 | 0.605580215599239                             | "(9.471204188481675, 36.675392670157066)"  | "(0.4706224549156486, 0.4350657642702081)"   | "(2054.320029604452, 348.7702913845563)"          | "(0.2833544868419934, 0.4350657642702081)"    |
| θ     | 946                 | 0.7112781954887218                            | "(9.0, 34.5)"                              | "(0.4444444444444444, 0.4855072463768116)"   | "(930.25, 272.25)"                                | "(0.17681999619844135, 0.4855072463768116)"   |
| ι     | 680                 | 0.5112781954887218                            | "(7.1647058823529415, 41.298529411764704)" | "(0.34248366013071896, 0.5840366581415175)"  | "(561.7597080449827, 542.8214727508649)"          | "(0.10677812355920599, 0.5840366581415175)"   |
| κ     | 872                 | 0.6556390977443609                            | "(8.869266055045872, 34.61009174311926)"   | "(0.4371814475025484, 0.4871027788857864)"   | "(1182.665789916674, 275.8951477148387)"          | "(0.22479866753785857, 0.4871027788857864)"   |
| λ     | 820                 | 0.6165413533834586                            | "(8.653658536585366, 35.21463414634146)"   | "(0.42520325203252035, 0.49586426299045594)" | "(1141.4509458655564, 296.34362879238535)"        | "(0.21696463521489381, 0.49586426299045594)"  |
| μ     | 679                 | 0.5105263157894737                            | "(6.989690721649485, 28.98379970544919)"   | "(0.33276059564719357, 0.40556231457172737)" | "(64.25946716235649, 120.64385596942572)"         | "(0.012214306626564624, 0.40556231457172737)" |
| ν     | 771                 | 0.5796992481203007                            | "(9.0, 31.69260700389105)"                 | "(0.4444444444444444, 0.44482039136073986)"  | "(372.7754243061969, 187.48748656300629)"         | "(0.07085638173468864, 0.44482039136073986)"  |
| ξ     | 715                 | 0.39285714285714285                           | "(13.213986013986014, 33.85314685314685)"  | "(0.48855944055944056, 0.4761325630890848)"  | "(970.1264609516357, 78.37820920338403)"          | "(0.17398250734426754, 0.4761325630890848)"   |
| ο     | 904                 | 0.6796992481203008                            | "(9.0, 34.5)"                              | "(0.4444444444444444, 0.4855072463768116)"   | "(930.25, 272.25)"                                | "(0.17681999619844135, 0.4855072463768116)"   |
| π     | 918                 | 0.5245714285714286                            | "(12.0, 31.781045751633986)"               | "(0.4583333333333333, 0.44610211234252156)"  | "(717.2244115511128, 60.5446729890213)"           | "(0.12981437313142313, 0.44610211234252156)"  |
| ρ     | 831                 | 0.6248120300751879                            | "(8.321299638989169, 31.468110709987965)"  | "(0.4067388688327316, 0.4415668218838835)"   | "(421.5584778173109, 181.39000609649253)"         | "(0.08012896366038984, 0.4415668218838835)"   |
| σ     | 953                 | 0.5236263736263737                            | "(9.631689401888773, 32.83735571878279)"   | "(0.3452675760755509, 0.46141095244612734)"  | "(774.9183734140488, 61.42414466273728)"          | "(0.13897388332389685, 0.46141095244612734)"  |
| τ     | 498                 | 0.3744360902255639                            | "(9.385542168674698, 29.487951807228917)"  | "(0.465863453815261, 0.4128688667714336)"    | "(599.659783713166, 131.97303672521414)"          | "(0.11398209156304238, 0.4128688667714336)"   |
| υ     | 872                 | 0.6556390977443609                            | "(9.0, 35.61811926605505)"                 | "(0.4444444444444444, 0.5017118734210876)"   | "(863.2949154637656, 310.39812647294)"            | "(0.16409331219611586, 0.5017118734210876)"   |
| φ     | 1096                | 0.48928571428571427                           | "(15.5, 34.5)"                             | "(0.46774193548387094, 0.4855072463768116)"  | "(306.25, 12.25)"                                 | "(0.05169648885887913, 0.4855072463768116)"   |
| χ     | 862                 | 0.6481203007518797                            | "(9.0, 34.57656612529002)"                 | "(0.4444444444444444, 0.48661690036652205)"  | "(1184.9727997265304, 274.78254450611263)"        | "(0.2252371791915093, 0.48661690036652205)"   |
| ψ     | 1034                | 0.4616071428571429                            | "(15.5, 31.988394584139265)"               | "(0.46774193548387094, 0.4491071678860763)"  | "(196.3250863297779, 0.9769238539558296)"         | "(0.03314062902258236, 0.4491071678860763)"   |
| ω     | 1340                | 0.5982142857142857                            | "(15.5, 36.13283582089552)"                | "(0.46774193548387094, 0.509171533636167)"   | "(833.3131677433727, 26.34600356426821)"          | "(0.1406673139337226, 0.509171533636167)"     |
