# Лабораторная работа №1. Передискретизация, обесцвечивание и бинаризация растровых изображений

**Выполнил:**

_Максимов Денис Б19-514_

**Задание:**

_Алгоритм адаптивной бинаризации Ниблэка._

### Предискретизация

#### Исходное изображение

![](./images/cat.bmp)

#### Растянутое в 2 раза изображение

![](./output_images/interpol_cat.png)


#### Сжатое в 10 раз изображение

![](./output_images/decim_cat.png)

#### Предискретизация в 2/10 раз изображение

![](./output_images/prediscr_cat.png)


### Приведение к полутоновому изображению

#### Усреднение

![](./output_images/semitone_avg_edition_cat.bmp)

#### Максимум

![](./output_images/semitone_max_edition_cat.bmp)

#### Фотошоп edition

![](./output_images/semitone_photoshop_edition_cat.bmp)

### Бинаризация

#### Исходное

![](./images/cat.bmp)

#### Бинаризованное

![](./output_images/binary_cat.bmp)

#### Исходное

![](./images/xmpl.bmp)

#### Бинаризованное

![](./output_images/binary_xmpl.bmp)